# KIT Matrix Dokumentation
**Website:** **https://docs.matrix.kit.edu**

### To build:
1. Clone the repository `git clone --recurse-submodules https://gitlab.kit.edu/kit/matrix4kit/documentation.git`
1. `cd documentation`
1. Install [Hugo](https://gohugo.io/getting-started/installing)
1. Run `hugo server -D`
1. Open http://localhost:1313/
