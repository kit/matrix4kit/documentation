---
title: "Element Mobile (iOS)"
date: 2020-10-12T09:29:07+02:00
draft: false
chapter: true
weight: 30
---

## Element Mobile (iOS)

Download for: {{% button href="https://apps.apple.com/app/vector/id1083446067" icon="fas fa-download" %}}iOS (iPhone/iPad){{% /button %}}

The following series of images shows screenshots of the setup of iOS Element. Since you are already registered at KIT, choose "I already have an account".

![Welcome screen of the Element iOS App](/images/15_iOS1_en.png?height=50vh&classes=border)

First, you need to enable logging in with KIT's Shibboleth service. To do so, choose the "Edit" button next to the field which still says "matrix.org".

![Screen for selecting the login method](/images/15_iOS2_en.png?height=50vh&classes=border)

Please enter **kit.edu** as server address.

![Server address prompt](/images/15_iOS3_en.png?height=50vh&classes=border)

You will be redirected to the usual KIT login page by pressing the "Continue using KIT-Account (SCC)".

![Weiterleitung zum KIT-Login](/images/15_iOS4_en.png?height=50vh&classes=border)

After successful login you will be prompted to confirm your matrix username. Just press "Continue".

![Seite zur Betstätigung des Nutzernamens](/images/15_iOS5_en.png?height=50vh&classes=border)