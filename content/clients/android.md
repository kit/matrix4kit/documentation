---
title: "Element Mobil (Android)"
date: 2020-10-12T09:29:07+02:00
draft: false
chapter: true
weight: 30
---

## Element Mobil (Android)

Downloads für: {{% button href="https://play.google.com/store/apps/details?id=im.vector.app" icon="fas fa-download" %}}Android (Google Play){{% /button %}} {{% button href="https://f-droid.org/packages/im.vector.app/" icon="fas fa-download" %}}Android (F-Droid){{% /button %}}

Da Sie am KIT bereits registriert sind, wählen Sie "Ich habe schon ein Konto".

![Willkommensbildschirm der Element Android App](/images/15_Android1_de.png?height=50vh&classes=border)

Zunächst müssen Sie den Zugang über den KIT-Login aktivieren. Wählen Sie hierzu den Knopf "Ändern"...

![Auswahl des Login-Mechanismus](/images/15_Android2_de.png?height=50vh&classes=border)

...und geben Sie als Serveradresse **matrix.scc.kit.edu** ein.

![Eingabefenster für die Serveradresse](/images/15_Android3_de.png?height=50vh&classes=border)

Nach einem Klick auf "Weiter" können Sie nun über den Knopf "Mit KIT-Account (SCC) weitermachen" zum gewohnten KIT-Login gelangen.

![Weiterleitung zum KIT-Login](/images/15_Android4_de.png?height=50vh&classes=border)

Sobald Sie sich angemeldet haben, werden Sie aufgefordert, Ihren Benutzernamen zu bestätigen. Klicken Sie hierzu auf "Continue".

![Bestätigung des Matrix-Nutzernamens](/images/15_Android5_de.png?height=50vh&classes=border)

Wir empfehlen, die Aufforderung zum "Verbessern der App" durch einen Klick auf "Nicht jetzt" zu überspringen.

![Aufforderung zur Verifizierung neuer Geräte](/images/15_Android6_de.png?height=50vh&classes=border)

Sie sollten nun angemeldet sein. Falls Sie sich bereits an einem anderen Gerät angemeldet haben, werden Sie aufgefordert die beiden Sitzungen gegenseitig zu bestätigen, damit verschlüsselte Nachrichten übertragen werden können. Weitere Informationen finden Sie [hier](/first-steps/#neue-geräte-verifizieren).

Bei Fragen melden Sie sich gerne im Raum [#helpdesk:kit.edu](https://matrix.to/#/#helpdesk:kit.edu).
