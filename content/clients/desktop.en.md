---
title: "Element Desktop"
date: 2020-10-12T09:27:07+02:00
draft: false
chapter: true
weight: 20
---

## Element Desktop

Downloads for: {{% button href="https://packages.riot.im/desktop/install/win32/x64/Element%20Setup.exe" icon="fas fa-download" %}}Windows{{% /button %}} 
{{% button href="https://packages.riot.im/desktop/install/macos/Element.dmg" icon="fas fa-download" %}}macOS{{% /button %}} 
{{% button href="/clients/install_linux/" icon="fas fa-download" %}}Linux{{% /button %}}

After a desktop installation, make sure to use the existing account with the KIT login and not to create a new account on another server. Here the example of Element:

![Selected login button in the element matrix client](/images/01_Desktop_Login_en.png)

This is done by clicking on **Edit**. Then you will not accidentally end up on the wrong server...

![Change login page with focus on the homeserver button](/images/02_Desktop_Change-Homeserver_en.png)

Now you can manually specify the home server: kit.edu

![Input field to change the home server with the input kit.edu](/images/03_Desktop_Set-Homeserver_en.png)

Afterwards the one-time login with KIT credentials must be carried out:

![Login with KIT credentials](/images/04_Desktop_Username_en.png)

By activating the slider under Settings > Settings > "**Automatic start after system login**", the element client starts after every reboot and you no longer miss any notifications due to accidentally closing the browser tab in the usage variant with the Web App.

![settings marked with the dot start automatically after system startup](/images/05_Desktop_Settings_en.png)

If you have any questions, please feel free to contact us in the [#helpdesk:kit.edu](https://matrix.to/#/#helpdesk:kit.edu) room.
