---
title: "Element Web (Browser)"
date: 2020-07-15T16:46:07+02:00
draft: false
chapter: true
weight: 10
---

# Using the Element webclient
Start here: [element.matrix.kit.edu](https://element.matrix.kit.edu) 

![Start page of Element Webclient with login button](/images/01_Browser_Welcome_en.png)

No registration is necessary, the service can be used immediately by clicking on "Sign In" on the homepage [element.matrix.kit.edu](https://element.matrix.kit.edu).

![Login window with a button referring to the KIT shibboleth login page](/images/02_Browser_Login_en.png)

To log-in click "Continue with KIT-Account (SCC)" and log-in with your KIT credentials.

After the first login there is also no e-mail / confirmation mail.

Matrix accounts of KIT members always have the form @ab1234:kit.edu for employees and @uabcd:kit.edu for students.

If you have any questions, please feel free to contact us in the [#helpdesk:kit.edu](https://matrix.to/#/#helpdesk:kit.edu) room.
