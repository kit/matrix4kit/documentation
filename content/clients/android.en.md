---
title: "Element Mobile (Android)"
date: 2020-10-12T09:29:07+02:00
draft: false
chapter: true
weight: 30
---

## Element Mobile (Android)

Downloads for: {{% button href="https://play.google.com/store/apps/details?id=im.vector.app" icon="fas fa-download" %}}Android (Google Play){{% /button %}} {{% button href="https://f-droid.org/packages/im.vector.app/" icon="fas fa-download" %}}Android (F-Driod){{% /button %}}

Since you are already registered at KIT, choose "I already have an account".

![Welcome screen of the Element android app](/images/15_Android1_de.png)

First, you need to activate the KIT login. To do so, click on "change"...

![Screen for choosing the login mechanism](/images/15_Android2_de.png)

...and enter **matrix.scc.kit.edu** as server address.

![Text field for entering the server address](/images/15_Android3_de.png)

After clicking the "Continue" button you will find the button "Continue with KIT-Account (SCC)", which will redirect you to the usual KIT login page.

![Redirect to the KIT login page](/images/15_Android4_de.png)

After logging in, you will be prompted to accept your matrix username. Choose "Continue".

![Accepting the matrix user name](/images/15_Android5_de.png)

We recommend to disable "improving the app" as it sends data to external parties. Choose "not now" to proceed to the app.

![Aufforderung zur Verifizierung neuer Geräte](/images/15_Android6_de.png)

In case you already logged in on another device, you will be asked to verify the two sessions so encrypted messages can be transferred safely. Further information on this can be found [here](/first-steps/#verify-new-devices).

If you have any questions, please feel free to contact us in the [#helpdesk:kit.edu](https://matrix.to/#/#helpdesk:kit.edu) room.
