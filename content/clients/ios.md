---
title: "Element Mobil (iOS)"
date: 2020-10-12T09:29:07+02:00
draft: false
chapter: true
weight: 30
---

## Element Mobil (iOS)

Download für: {{% button href="https://apps.apple.com/app/vector/id1083446067" icon="fas fa-download" %}}iOS (iPhone/iPad){{% /button %}}

Die folgende Bilderreihe zeigt Bildschirmfotos der Einrichtung von iOS Element. Da Sie bereits am KIT registriert sind, wählen Sie "Ich habe bereits ein Konto".

![Willkommensbildschirm der Element iOS App](/images/15_iOS1_de.png?height=50vh&classes=border)

Zunächst muss die Anmeldung über den KIT-Login aktiviert werden. Wählen Sie hierzu den Knopf "Bearbeiten" rechts neben dem Feld, in dem noch "matrix.org" steht.

![Auswahlbildschirm für die Login-Methode](/images/15_iOS2_de.png?height=50vh&classes=border)

Geben Sie nun als Server-Adresse **kit.edu** ein, und drücken Sie "Bestätigen".

![Bildschirm zur Eingabe der Serveradresse](/images/15_iOS3_de.png?height=50vh&classes=border)

Über den Knopf "Weiter mit KIT-Account (SCC)" gelangen Sie zur gewohnten KIT-Login-Seite.

![Weiterleitung zum KIT-Login](/images/15_iOS4_de.png?height=50vh&classes=border)

Nach erfolgreicher Anmeldung werden Sie zur Bestätigung des Matrix-Nutzernamens aufgefordert. Wählen Sie einfach "Continue".

![Seite zur Betstätigung des Nutzernamens](/images/15_iOS5_de.png?height=50vh&classes=border)