---
title: "Element Web (Browser)"
date: 2020-07-15T16:46:07+02:00
draft: false
chapter: true
weight: 10
---

## Nutzung des Webclients

Starten Sie auf [element.matrix.kit.edu](https://element.matrix.kit.edu).

![Startseite von Element Webclient mit Anmeldebutton](/images/01_Browser_Welcome_de.png)

Hierzu ist keine Registrierung nötig, der Dienst kann sofort durch Klick auf „Anmelden“ auf der Startseite [element.matrix.kit.edu](https://element.matrix.kit.edu) genutzt werden.

![Loginfenster einem Knopf welcher zum KIT Shibboleth login weiterleitet](/images/02_Browser_Login_de.png)

Klicken Sie auf "Mit KIT-Account (SCC) fortfahren" und loggen Sie sich mit Ihren KIT Zugangsdaten ein.

Es folgt nach dem Erstlogin auch keine E-Mail / Bestätigungsmail.

Matrix-Accounts von KIT-Angehörigen haben immer die Form @kuerzel:kit.edu, also für Mitarbeiter zum Beispiel @ab1234:kit.edu oder für Studierende @uabcd:kit.edu.

Bei Fragen melden Sie sich gerne im Raum [#helpdesk:kit.edu](https://matrix.to/#/#helpdesk:kit.edu).
