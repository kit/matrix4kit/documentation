---
title: "Element Desktop"
date: 2020-10-12T09:27:07+02:00
draft: false
chapter: true
weight: 5
---

## Element Desktop

Downloads für: 
{{% button href="https://packages.riot.im/desktop/install/win32/x64/Element%20Setup.exe" icon="fas fa-download" %}}Windows{{% /button %}} 
{{% button href="https://packages.riot.im/desktop/install/macos/Element.dmg" icon="fas fa-download" %}}macOS{{% /button %}} 
{{% button href="/clients/install_linux/" icon="fas fa-download" %}}Linux{{% /button %}}

Nach einer Desktop-Installation ist darauf zu achten, den bestehenden Account mit dem KIT-Login zu nutzen, und keinen neuen Account auf einem anderen Server zu erstellen. Hier am Beispiel von Element:

![Anmeldebutton im Element Matrixclient](/images/01_Desktop_Login_de.png)

Dies wird durch Klick auf **Bearbeiten** realisiert. Dann landet man nicht versehentlich auf einem falschen Server...

![Anmeldeseite mit Fokus auf dem Homeserver ändern Button](/images/02_Desktop_Change-Homeserver_de.png)

Nun kann man manuell die Angabe des Heimservers durchführen: kit.edu

![Eingabefeld zum Ändern des Homeservers mit der Eingabe kit.edu](/images/03_Desktop_Set-Homeserver_de.png)

Anschließend ist der einmalige Login mit den KIT Zugangsdaten durchzuführen:

![Login mit KIT Account (SCC) durchführen](/images/04_Desktop_Username_de.png)

Mit der Aktivierung des Schiebereglers unter Alle Einstellungen > Optionen > Allgemein > „**Nach System-Login automatisch starten**“ startet der Element-Client nach jedem Neustart und man verpasst keine Benachrichtigungen mehr durch ein versehentliches Schließen des Browser-Tabs in der Nutzungsvariante mit der Web-App.

![Einstellungen mit dem Punkt nach Systemstart automatisch starten markiert](/images/05_Desktop_Settings_de.png)

Bei Fragen melden Sie sich gerne im Raum [#helpdesk:kit.edu](https://matrix.to/#/#helpdesk:kit.edu).
