---
title: "Clients / Installation"
date: 2020-07-02T21:22:27+02:00
draft: false
chapter: true
weight: 15
---

# Installation of a client / program

The recommended client for using Matrix is called Element and can be used on different systems and devices:
1. **Element Web:** [The KIT browser web application](#web-app)
1. **Element Desktop:** [Downloads for Linux/Windows/Mac](#desktop-app)
1. **Element mobile:** [Android/iOS](#mobile-app)

![Element page for client download](/images/12_Element-Download.png)

## Element Web {#web-app}

Web-App: {{% button href="https://element.matrix.kit.edu" icon="fas fa-globe" %}}element.matrix.kit.edu{{% /button %}}

The easiest way is to open the Element Web application directly in a modern browser (e.g. [Mozilla Firefox](https://www.mozilla.org/de/firefox/)).

Further information can be found in the submenu ["Element Web (Browser)"](/en/clients/browser).

## Element Desktop {#desktop-app}

Downloads for: {{% button href="https://packages.riot.im/desktop/install/win32/x64/Element%20Setup.exe" icon="fas fa-download" %}}Windows{{% /button %}} {{% button href="https://packages.riot.im/desktop/install/macos/Element.dmg" icon="fas fa-download" %}}macOS{{% /button %}} {{% button href="/clients/install_linux" icon="fas fa-download" %}}Linux{{% /button %}}

More recommendable than using a browser tab is the installation of the program Element on your own computer. Here you can keep the overview independently of the browser (however, you should also take care of the updates of the program).

More information can be found in the submenu ["Element Desktop"](/en/clients/desktop).

## Element Mobile {#mobile-app}

Downloads for: {{% button href="https://play.google.com/store/apps/details?id=im.vector.app" icon="fas fa-download" %}}Android (Google Play){{% /button %}} {{% button href="https://apps.apple.com/app/vector/id1083446067" icon="fas fa-download" %}}iOS (iPhone/iPad){{% /button %}} {{% button href="https://f-droid.org/packages/im.vector.app/" icon="fas fa-download" %}}Android (F-Driod){{% /button %}}

To have access to Matrix on the road, there is the possibility to install a mobile element client on the smartphone.

Further information can be found in the submenu ["Element Android"](/en/clients/android) or ["Element iOS"](/en/clients/ios).
