---
title: "Datenschutzerklärung"
date: 2022-10-04T19:02:20+02:00
draft: false
chapter: true
weight: 200
---

## Informationen zum Datenschutz beim Matrix Dienst
Die folgenden Ausführungen dienen Ihrer Information über die Verarbeitung Ihrer personenbezogenen Daten bei Nutzung des Matrix-Dienstes. “Matrix” ist ein offener, dezentraler Dienst für die Echtzeitkommunikation. Es wird den Mitgliedern und Angehörigen des KIT unter Einhaltung der einschlägigen gesetzlichen und rechtlichen Bestimmungen zum Datenschutz und zur IT-Sicherheit ermöglicht, mit Angehörigen dieser und anderer Universitäten sowie weiteren Matrix-Nutzenden (bspw. akademischen Partner:innen) per Chat sowie Audio-/Video-Telefonie zu kommunizieren.

### I. Verarbeitung personenbezogener Daten
Die Datenverarbeitung umfasst folgende Kategorien von Daten:
- KIT-Kürzel als Matrix-ID
- Anzeigename, voreingestellt auf KIT-Kürzel und bei Bedarf frei änderbar
- E-Mail-Adresse, bestehend aus KIT-Kürzel und Endung „@sysmail.kit.edu“
- Geräte- und Protokollinformationen, insbesondere IP-Adresse und Daten wie Art des Endgeräts und Betriebssystem

Zudem werden alle von Ihnen in das System eingegebenen Daten verarbeitet, eine Ende-zu-Ende-Verschlüsselung ist möglich.

Die Daten werden im Rahmen des auf Servern des KIT betriebenen Matrix-Dienstes verarbeitet. Wird mit Nutzenden kommuniziert, die bei externen Anbietern registriert sind, werden personenbezogene Daten (insbesondere Matrix-ID, Anzeigename und Profilbild) zu den externen Anbietern übermittelt.

Ihre personenbezogenen Daten, die zwecks Verwaltung Ihres Matrix-Kontos verarbeitet werden, bleiben gespeichert, solange der Account besteht. Nach Einleitung der Löschung des Accounts werden diese personenbezogenen Daten nach 30 Tagen gelöscht.

Die Aufbewahrungsdauer für die nutzungsbezogenen Daten richten sich nach der Erforderlichkeit für die Dienste. Diese werden mit Wegfall der Erforderlichkeit nach 30 Tagen gelöscht. Die systemgenerierten Protokolldaten werden nach einem Zeitraum von 7 Tagen gelöscht.

### II. Verantwortliche
Verantwortlicher für die Datenverarbeitung im Sinne der DS-GVO sowie anderer datenschutzrechtlicher Bestimmungen ist:

```plaintext
Karlsruher Institut für Technologie
Kaiserstraße 12
76131 Karlsruhe
Deutschland
Tel.: +49 721 608-0
Fax: +49 721 608-44290
E-Mail: info@kit.edu
```
Das Karlsruher Institut für Technologie ist Körperschaft des öffentlichen Rechts. Es wird vertreten durch [den Präsidenten bzw. die Präsidentin des KIT](https://www.kit.edu/ps/praesident.php).

Unsere Datenschutzbeauftragte bzw. unseren Datenschutzbeauftragten erreichen Sie unter dsb@kit.edu oder der Postadresse mit dem Zusatz „Die/Der Datenschutzbeauftragte“.

### III. Rechtsgrundlage
Bei Nutzung im Beschäftigtenkontext ist die Rechtsgrundlage Artikel 88 Absatz 1 DS-GVO in Verbindung mit § 15 Absatz 1 LDSG, da die Datenverarbeitung für die Durchführung des Dienstverhältnisses erforderlich ist.

Bei Nutzung des Dienstes für Hochschulaufgaben des KIT ergibt sich die Rechtsgrundlage aus Artikel 6 Absatz 1 Buchstabe e, Absatz 3 Buchstabe b DS-GVO in Verbindung mit § 12 Landeshochschulgesetz in Verbindung mit §§ 2, 20 KIT-Gesetz.

Bei Nutzung zur Erfüllung der übrigen Aufgaben des KIT ergibt sich die Rechtsgrundlage aus Artikel 6 Absatz 1 Buchstabe e, Absatz 3 Buchstabe b DS-GVO in Verbindung mit § 4 Landesdatenschutzgesetz (LDSG) in Verbindung mit § 2 KIT-Gesetz.

### IV. Ihre Rechte
Hinsichtlich der Sie betreffenden personenbezogenen Daten haben Sie gegenüber uns folgende Rechte:
- Recht auf Bestätigung, ob Sie betreffende Daten verarbeitet werden und auf Auskunft über die verarbeiteten Daten, auf weitere Informationen über die Datenverarbeitung sowie auf Kopien der Daten (Artikel 15 DS-GVO)
- Recht auf Berichtigung oder Vervollständigung unrichtiger bzw. unvollständiger Daten (Artikel 16 DS-GVO)
- Recht auf unverzügliche Löschung der Sie betreffenden Daten (Artikel 17 DS-GVO)
- Recht auf Einschränkung der Verarbeitung (Artikel 18 DS-GVO)
- Recht auf Widerspruch gegen die künftige Verarbeitung der Sie betreffenden Daten, sofern die Daten nach Maßgabe von Artikel 6 Absatz 1 Unterabsatz 1 Buchstabe e oder f DS-GVO verarbeitet werden (Artikel 21 DS-GVO)

Sie haben zudem das Recht, sich bei der Aufsichtsbehörde über die Verarbeitung der Sie betreffenden personenbezogenen Daten durch das Karlsruher Institut für Technologie (KIT) zu beschweren (Artikel 77 DS-GVO).

Aufsichtsbehörde im Sinne des Artikels 51 Absatz 1 DS-GVO über das KIT ist gemäß § 25 Absatz 1 LDSG: Der Landesbeauftragte für den Datenschutz und die Informationsfreiheit Baden-Württemberg

Sie erreichen die Aufsichtsbehörde unter: https://www.baden-wuerttemberg.datenschutz.de/kontakt-aufnehmen/
