---
title: "Privacy Policy"
date: 2022-10-04T19:02:20+02:00
draft: false
chapter: true
weight: 200
---

## Privacy policy for the use of Matrix
The following explains the processing of personal data when using the Matrix service. "Matrix" is an open, decentralized service for real-time communication. In compliance with legal regulations for data protection and IT security, it enables KIT members and affiliates to communicate with each other, members of other universities and all other Matrix users (e.g. academic partners) via chat and audio/video telephony.

## I. Processing of personal data
Data processing includes the following categories of data:
- KIT account ID used as matrix ID
- Display name, preset to KIT account and arbitrarily changeable
- Mail address, generated from the KIT account ID by concatenating "@sysmail.kit.edu"
- Device and protocol information, in particular the IP address and data such as device type and operating system of the user's device

In addition, all data that you enter into the system will be processed. End-to-end encryption is possible.

The data is processed as part of the Matrix service operated on servers run at KIT. When communicating with users that are registered with external providers, personal data (in particular Matrix ID, display name and profile picture) will be transmitted to the external providers.

Your personal data that is processed for the purpose of managing your Matrix account will remain stored as long as the account exists. After initiating the deletion of the account, the personal data will be deleted after 30 days.
The retention period for usage-related data is based on the necessity for the services. Once the personal data is no longer necessary in relation to the purposes for which it was collected, it will be deleted after 30 days.
System-generated log data is deleted after 7 days.

## II. Controller
The controller of data processing within the meaning of the GDPR and other data protection legislation:

```plaintext
Karlsruhe Institute of Technology
Kaiserstraße 12
76131 Karlsruhe
Germany
Phone: +49 721 608-0
Fax: +49 721 608-44290
E-mail: info∂kit.edu
```
KIT is a corporation governed by public law. It is represented by the [President of KIT](https://www.kit.edu/eb/president.php).

Our Data Protection Officer can be contacted at dsb∂kit.edu or by ordinary mail with “Die/Der Datenschutzbeauftragte” (the data protection officer) being indicated on the envelope.

## III. Legal basis
When used in the employment context, the legal basis is Article 88(1) GDPR in conjunction with Section 15(1) Landesdatenschutzgesetz Baden-Württemberg (LDSG, State data protection act of the state Baden-Württemberg), as the data processing is necessary for the performance of the employment relationship.

When using Microsoft Teams for tasks of higher education, the legal basis is Article 6(1)(e), (3)(b) GDPR in conjunction with Section 12 Landeshochschulgesetz (LHG, Higher Education Act of the state Baden-Württemberg) in conjunction with Sections 2, 20 KIT-Gesetz.

When Microsoft Teams is used to fulfill the other tasks of KIT, the legal basis is Article 6(1)(e), (3)(b) GDPR in conjunction with Section 4 LDSG in conjunction with Section 2 KIT-Gesetz.

### IV. Rights
You have the following rights regarding your personal data:
- the right to obtain confirmation as to whether data concerning you is being processed and information on the data processed, further information on data processing, and copies of data (Article 15 GDPR)
- the right to rectify or complete inaccurate or incomplete data (Article 16 GDPR)
- the right to erasure of data concerning you without undue delay (Article 17 GDPR)
- the right to restriction of processing (Article 18 GDPR)
- the right to object to the future processing of the data concerning you which is based on Article 6(1) point (e) or (f) GDPR (Article 21 GDPR)
Please note that the above rights may be restricted in certain cases (see in particular Sections 8 to 11 LDSG or 13 (4) LDSG).

You also have the right to lodge a complaint with a supervisory authority about the processing of the personal data concerning you by Karlsruhe Institute of Technology (KIT) (Article 77 GDPR). 

Supervisory authority of KIT in the sense of Article 51(1) GDPR is according to Section 25(1) LDSG: The State Commissioner for Data Protection and Freedom of Information Baden-Württemberg (Landesbeauftragter für den Datenschutz und die Informationsfreiheit Baden-Württemberg); reachable under: https://www.baden-wuerttemberg.datenschutz.de/kontakt-aufnehmen/
