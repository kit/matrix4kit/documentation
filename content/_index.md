---
title: "Matrix am KIT"
date: 2020-08-02T21:26:25+02:00
chapter: false
draft: false
---

## Matrix am KIT
Matrix ist ein freies und offenes, sicheres, dezentralisiertes Protokoll für Echtzeit-Kommunikation, das auch unter dem 
Namen eines seiner nutzenden Programme, Element, bekannt ist.

<object data="/images/matrix_interactive.svg" type="image/svg+xml" style="width: 1280px; max-width: 100%"></object>

Zur Zusammenarbeit in Teams stieg in den letzten Jahren der Bedarf an unterstützenden digitalen Werkzeugen (engl. *tool*). 
Ein zentrales Werkzeug ist dabei ein Team-Chat. Ein Chat bezeichnet, laut Wikipedia, „die elektronische Kommunikation 
mittels geschriebenem Text in Echtzeit, meist über das Internet“ ([Quelle](https://de.wikipedia.org/wiki/Chat)). 
Die dazugehörige Handlung nennt man „*chatten*“. Mit einem Chattool (manchmal auch Messenger genannt) können sich 
Teammitglieder gegenseitig auf aktuelle Informationen aufmerksam machen und insb. Verknüpfungen (*Hyperlinks* / *Links*) 
zur weitergehenden Zusammenarbeit teilen (bspw. zur Terminfindung, zum kollaborativen Schreiben, zum Planen von Events, 
zum Bearbeiten von Daten, Code, Mindmaps oder Prozessen).

## Themen der Dokumentation

* [Warum Matrix und kein anderes Chat-System?](/why/)
* [Wie kann Matrix genutzt werden? (Anmeldung und erste Schritte)](/first-steps/)

* [Empfehlungen zu weiteren wichtigen Einstellungen nach dem Erstlogin](/settings/)

* [Installation eines Clients / Programms](/clients/)

    * [Browsereinstellungen](/clients/browser/)

* [Personen finden und direkte Nachrichten versenden](/messaging/)

* [Ende-zu-Ende-Verschlüsselung nutzen](/encryption/)

* [Räume erstellen und Verantwortung übernehmen](/rooms/)

    * [Räume teilen und publik machen](/rooms/sharing/)

    * [Räume finden](/rooms/find/)

    * [Räume löschen und aus Räumen austreten](/rooms/delete/)

* [Benachrichtigungen feiner einstellen](/notifications/)

* [Weitere Clients](/clients/more_clients/)

* [Spaces zur Raumverwaltung einsetzen](/spaces/)

* [Weiterentwicklung von Matrix](/development/)

### Danksagung für diese Dokumentation
Die Grundlagen dieser Dokumentation wurden von der [TU Dresden](https://doc.matrix.tu-dresden.de) erstellt und
dankenswerterweise unter einer Creative Commons Lizenz veröffentlicht. Die Quelle der Dokumentation finden sie 
[hier](https://github.com/matrix-tu-dresden-de/Dokumentation). Diese Dokumentation und alle Änderungen, die gemacht wurden,
um die Dokumentation der TU Dresden für das KIT anzupassen, finden Sie [hier](https://gitlab.kit.edu/kit/matrix4kit/documentation).

### Fragen / Kontakt

Allgemeine Fragen richten Sie bitte an den [Service Desk](https://www.scc.kit.edu/dienste/servicedesk.php).

{{% notice tip %}}
Man kann bei manchen Anomalien probieren den Cache (Zwischenspeicher) zu leeren und alles neu zu laden: Einstellungen > Hilfe & Über > Cache löschen und neu laden
{{% /notice %}}
