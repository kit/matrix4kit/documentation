---
title: "Erste Schritte"
date: 2020-08-02T21:26:25+02:00
chapter: true
draft: false
weight: 2
---

# Erste Schritte - Wie kann Matrix genutzt werden?

## Matrix-Login mit KIT-Account

Mitgliedern und Angehörigen des KIT (selbstverständlich auch Studierenden) wird unter Einhaltung der einschlägigen 
gesetzlichen und rechtlichen Bestimmungen zum Datenschutz und zur IT-Sicherheit ermöglicht, 
mittels ihres **KIT-Logins** mit Angehörigen dieser und anderer Hochschulen und Universitäten sowie weiteren 
Matrix-Nutzenden (bspw. akademischen Partner:innen) per Chat sowie Audio-/Video-Telefonie zu kommunizieren.

{{% notice tip %}}
Wir empfehlen die Nutzung eines [Desktop / mobile Clients](https://matrix.org/clients/) oder den vom SCC betriebenen [Element Webclient](https://element.matrix.kit.edu).
{{% /notice %}}

Starten Sie auf [element.matrix.kit.edu](https://element.matrix.kit.edu).

![Startseite von Element Webclient mit Anmeldebutton](/images/01_Browser_Welcome_de.png)

Hierzu ist keine Registrierung nötig, der Dienst kann sofort durch Klick auf „Anmelden“ auf der Startseite [element.matrix.kit.edu](https://element.matrix.kit.edu) genutzt werden.

![Loginfenster einem Knopf welcher zum KIT Shibboleth login weiterleitet](/images/02_Browser_Login_de.png)

Klicken Sie auf "Mit KIT-Account (SCC) fortfahren" und loggen Sie sich mit Ihren KIT Zugangsdaten ein.

Es folgt nach dem Erstlogin auch keine E-Mail / Bestätigungsmail.

Für die Matrix-Adressen ergibt sich folgende Struktur:

Für Studierende: @uxxxx:kit.edu<br>
Für Mitarbeitende/Partner: @ab1234:kit.edu

## Bequemes Nutzen der Ende-zu-Ende-Verschlüsselung (E2EE)

Matrix verschlüsselt nicht nur die Transporte von und zu dem Heimserver (im Rechenzentrum des KIT) sondern erlaubt auch 
die Nutzung von Ende-zu-Ende-Verschlüsselung (E2EE). Hierzu müssen kryptografische Schlüssel zwischen allen Geräten 
ausgetauscht werden, die sich Ende-zu-Ende-verschlüsselt schreiben möchten. Obwohl diese technische Notwendigkeit 
kompliziert klingt und im Hintergrund auch ist, ist sie inzwischen für die Anwendenden sehr bequem geworden. Die vielen 
kryptografischen Schlüssel werden vom Client erstellt auf dem jeweiligen Gerät gespeichert. Sollte dies bspw. ein Tab 
in einem Browser sein, besteht die Gefahr, dass dieser Tab einmal unbeabsichtigt geschlossen wird. Dann sind alle 
verschlüsselten Inhalte nicht mehr lesbar. Damit dies nicht geschieht, wird eine Schlüsselsicherung auf dem 
Heimserver des KIT angeboten, auf der (mit einer Sicherheitsphrase (bzw. daraus errechenbaren Sicherheitsschlüssel) geschützt) 
alle kryptografischen Schlüssel verschlüsselt abgelegt sind.

{{% notice info %}}
Es wird dringend empfohlen, die Schlüsselsicherung zu nutzen (mit einer sicheren Sicherheitsphrase, welche NICHT Ihr KIT-Passwort ist)!
{{% /notice %}}

Drücken Sie nach dem ersten Login oben links auf Fortfahren, um die Schlüsselsicherung einzurichten.
![Nach dem Login auf Fortfahren bei "Schlüsselsicherung einrichten" drücken](/images/01_Browser_After_Login_de.png)
Drücken Sie auf Sicherheitsphrase eingeben.
![Aufforderung den Sicherheitsschlüssel zu generieren oder eine Sicherheitsphrase einzugeben](/images/02_Browser_Setup_Encryption_Keys_de.png)
Geben Sie nun eine Sicherheitsphrase ein, die NICHT Ihr KIT-Passwort ist. Bitte merken Sie sich diese sehr gut. 
Sie gewährt Zugang zu Ihren verschlüsselten Nachrichten.
![Aufforderung eine Passwort für die Schlüsselsicherung einzugeben](/images/03_Browser_Security_Phrase_de.png)
Alternativ können Sie sich statt der Sicherheitsphrase auch einen Sicherheitsschlüssel generieren lassen, 
welcher den selben Zweck wie die Sicherheitsphrase erfüllt. Weiterhin wird der Sicherheitsschlüssel immer zusätzlich 
zur Sicherheitsphrase erstellt und sollte als Notfallschlüssel sicher und wiederauffindbar verwahrt werden (z.B. Abspeichern als .txt-Datei UND Ausdrucken) 
![Anzeige des Sicherheitsschlüssel zum abschreiben oder wegspeichern](/images/04_Browser_Security_Key_de.png) 

[Weitere wichtige Einstellungen](/settings/) können Ihr Matrix-Erlebnis verbessern!


## Neue Geräte verifizieren

Wenn Sie sich auf weiteren Geräten anmelden wollen, müssen Sie diese verifizieren. Dazu stehen zwei Möglichkeiten zur Verfügung.
Sie können das neue Gerät entweder mit einem bereits angemeldeten Gerät oder mit Ihrer Sicherheitsphrase bzw. Ihrem Sicherheitsschlüssel verifizieren.

Klicken Sie auf "Mit anderem Gerät verifizieren".
![Anzeige Gerät verifizieren](/images/05_Browser_Verify_New_Device_de.png)
Bestätigen Sie nun auf Ihrem anderen Gerät die Verifizierungsanfrage.

Folgen Sie nun dem Dialog, der Sie durch die Schritte zur Verifizierung leitet.

Bei Fragen melden Sie sich gerne im Raum [#helpdesk:kit.edu](https://matrix.to/#/#helpdesk:kit.edu).
