---
title: "First steps"
date: 2020-08-02T21:26:25+02:00
chapter: true
draft: false
weight: 2
---

# First steps - How to use Matrix?

## Matrix-Login with KIT account

Members and members of the KIT (of course also students) can use their **KIT login** to communicate with members of the
KIT and other universities and other Matrix users (e.g. academic partners) via chat and audio/video telephony in 
compliance with the relevant legal and regulatory provisions on data protection and IT security.

{{% notice tip %}}
We recommend using a [desktop / mobile client](https://matrix.org/clients/) or the [Element web client](https://element.matrix.kit.edu) provided by SCC.
{{% /notice %}}

Start here: [element.matrix.kit.edu](https://element.matrix.kit.edu)

![Start page of Element Webclient with login button](/images/01_Browser_Welcome_en.png)

No registration is necessary, the service can be used immediately by clicking on "Sign In" on the homepage [element.matrix.kit.edu](https://element.matrix.kit.edu).

![Login window with a button referring to the KIT shibboleth login page](/images/02_Browser_Login_en.png)

To log-in click "Continue with KIT-Account (SCC)" and log-in with your KIT credentials.

After the first login there is also no e-mail / confirmation mail.

The matrix addresses have the following structure:

For students: @uxxxx:kit.edu<br>
For employees/partners: @ab1234:kit.edu

## Convenient use of end-to-end encryption (E2EE)

Matrix not only encrypts transports to and from the home server (in the data center of KIT) but also allows the use of 
end-to-end encryption (E2EE). For this, cryptographic keys have to be exchanged between all devices that want to write 
end-to-end encrypted. This technical necessity sounds and is complicated, but in the meantime it has become very 
convenient for the users. The many cryptographic keys created by the client are stored on the respective device. 
If this is a tab in a browser, for example, there is a risk that this tab will be closed unintentionally. Then all 
encrypted contents are no longer readable. To prevent this from happening, a key protection is offered on the home 
server of the KIT, on which (protected with a security phrase (or security key that can be calculated from it) 
all cryptographic keys are stored encrypted. 
   
{{% notice info %}}
It is highly recommended to use this key backup (with a secure security phrase which is NOT your KIT password)!
{{% /notice %}}

After the first login, press Continue in the upper left corner to set up the key backup.
![After logging in, press Continue on "Set up Secure Backup"](/images/01_Browser_After_Login_en.png)
Press Enter a Security Phrase.
![Prompt to generate a security key or enter a security phrase](/images/02_Browser_Setup_Encryption_Keys_en.png)
Now enter a security phrase that is NOT your KIT password. Please remember it very well.
It grants access to your encrypted messages.
![Prompt to enter a security phrase for the key backup](/images/03_Browser_Security_Phrase_en.png)
Alternatively, instead of the security phrase, you can also have a security key generated that serves the same purpose as the security phrase. 
Furthermore, the security key is generated in addition to the security phrase and should be kept safe and retrievable as an emergency key (e.g. save it as .txt file AND print it out) 
![Display of the security key to write or save away](/images/04_Browser_Security_Key_en.png) 

[Other important settings](/en/settings/) may improve your Matrix experience!


## Verify New Devices

If you want to log in on additional devices, you must verify them. There are two ways to do this.
You can either verify the new device with an already logged in device or with your security phrase or security key.

Click on "Verify with another device".
![Display Verify Device](/images/05_Browser_Verify_New_Device_en.png)
Now confirm the verification request on your other device.

Now follow the dialog that guides you through the steps to verify.

If you have any questions, please feel free to contact us in the [#helpdesk:kit.edu](https://matrix.to/#/#helpdesk:kit.edu) room.
