---
title: "Matrix at KIT"
date: 2020-08-02T21:26:25+02:00
chapter: false
draft: false
---
## Matrix at KIT
Matrix is a free and open, secure, decentralized protocol for real-time communication, also known by the name of one of its clients, Element.

<object data="/images/matrix_interactive_en.svg" type="image/svg+xml" style="width: 1280px; max-width: 100%"></object>

In recent years, the need for supporting digital tools for team collaboration has increased. A central tool is a team chat. 
According to Wikipedia, a chat refers to "electronic communication by means of written text in real time, usually via 
the Internet". ([Source](https://en.wikipedia.org/wiki/Chat)). With a messenger team members can make each other 
aware of current information and especially share links (*hyperlinks* / *links*) for further collaboration 
(e.g. for finding appointments, collaborative writing, planning events, editing data, code, mind maps,). 

## Topics

* [Why Matrix and no other chat system?](/en/why/)

* [How can Matrix be used? (registration and first steps)](/en/first-steps/)

* [Recommendations for further important settings after first login](/en/settings/)

* [Installation of a client / program](/en/clients/)

    * [Browsersettings](/en/clients/browser/)

* [Find people and send direct messages](/en/messaging/)

* [Use end-to-end encryption](/en/encryption/)

* [Create rooms and take responsibility](/en/rooms/)

    * [Sharing rooms and making them public](/en/rooms/sharing/)

    * [Find rooms](/en/rooms/find/)

    * [Delete and leave rooms](/en/rooms/delete/)

* [Fine-tune notifications](/en/notifications/)

* [More Clients](/en/clients/more_clients/)

* [Using Spaces for managing rooms](/en/spaces/)

* [Further development of Matrix](/en/development/)

### Acknowledgements for this documentation
The basic principles of this documentation were created by the [TU Dresden](https://doc.matrix.tu-dresden.de) and
thankfully published under a Creative Commons licence. The source of the documentation can be found
[here](https://github.com/matrix-tu-dresden-de/Dokumentation). This documentation and all changes that have been made
to adapt the TU Dresden documentation for KIT can be found [here](https://gitlab.kit.edu/kit/matrix4kit/documentation).

### Questions and Contact

Please address general questions to the [Service Desk](https://www.scc.kit.edu/en/services/servicedesk.php).

{{% notice tip %}}
For some anomalies you can try to empty the cache and reload everything: Settings > Help & About > Clear Cache and reload
{{% /notice %}}
