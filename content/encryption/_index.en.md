---
title: "Encryption"
date: 2020-07-03T13:20:58+02:00
draft: false
chapter: true
weight: 60
---

# Use of end-to-end encryption

1:1 calls are encrypted end-to-end by default. Therefore, a configured key backup as well as a verification of all self
used client devices is recommended ([Important settings](/en/settings))

If you want to verify the authenticity of the chat with other people, you can perform a key verification.
To do this, click on the corresponding person in the list of persons in the room / private chat.

![open the list of persons in the room](/images/01_Verification_en.png)

Then click on the person you want to verify.

To start the verification process, click on "Verify".
You must now wait for the other person to accept the request on their end.

![open person](/images/02_Verification_en.png)

Once the request is accepted, you can select a verification method. Usually this is "Verify by emojis".

![open person](/images/03_Verification_en.png)

Now compare the emojis and click "They match" if the displayed emojis are the same.

![open person](/images/04_Verification_en.png)

This completes the verification process.

In the respective room rows the following symbols indicate the status of the encryption and the corresponding verification:

![symbol for at least one non-verified person](/images/gray.png)

At least one person in the room has not yet been verified.

![symbol for a verified person who has opened unverified sessions](/images/unverified.png)

There is at least one person in the room who has already been verified, but who in turn has opened further unverified sessions. 

![symbol for all persons in the room are verified](/images/green.png)

All persons in the room were verified.

