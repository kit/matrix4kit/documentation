---
title: "Verschlüsselung"
date: 2020-07-03T13:20:58+02:00
draft: false
chapter: true
weight: 60
---

# Ende-zu-Ende-Verschlüsselung nutzen

1:1 Gespräche sind seit standardmäßig Ende-zu-Ende-verschlüsselt. Daher wird eine eingerichtete Schlüsselsicherung 
sowie eine Verifikation aller selbst eingesetzten Client-Geräte empfohlen ([Wichtige Einstellungen](/settings))

Wenn Sie die Authentizität des Chats mit anderen Personen verifizieren wollen, können Sie eine Schlüsselverifizierung durchführen.
Klicken Sie dafür in der Personenliste des Raumes / privaten Chats auf die entsprechende Person.

![öffnen der Personenliste in dem Raum](/images/01_Verification_de.png)

Klicken Sie anschließend auf die Person, die Sie verifizieren möchten.

Um den Verifizierungsprozess zu starten, klicken Sie auf "Verifizieren".
Sie müssen nun darauf warten, dass die andere Person die Anfrage bei sich annimmt.

![öffnen Person](/images/02_Verification_de.png)

Sobald die Anfrage angenommen wurde, können Sie eine Verifizierungsmethode auswählen. In der Regel ist dies "Mit Emojis verifizieren".

![öffnen Person](/images/03_Verification_de.png)

Vergleichen Sie nun die Emojis und klicken Sie auf "Sie passen zueinander", wenn die angezeigten Emojis dieselben sind.

![öffnen Person](/images/04_Verification_de.png)

Damit ist der Verifizierungsprozess beendet.

In den jeweiligen Raumzeilen deuten folgende Symbole den Status der Verschlüsselung und der dazugehörigen Verifikation an:

![Symbol für mindestens eine nicht-verifizierte Person](/images/gray.png)

Mindestens eine Person im Raum wurde noch nicht verifiziert.

![Symbol für eine verifizierte Person, die unverifizierte Sitzungen geöffnet hat](/images/unverified.png)

Im Raum ist mindestens eine Person, die bereits verifiziert wurde, aber die ihrerseits weitere unverifizierte Sitzungen geöffnet hat. 

![Symbol für alle Personen im Raum sind verifiziert](/images/green.png)

Alle im Raum befindlichen Personen wurden verifiziert.
