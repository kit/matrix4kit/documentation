---
title: "Nachrichten suchen"
date: 2021-03-02T11:42:35+01:00
draft: false
chapter: true
weight: 40
---

## Nachrichten suchen

Das Suchen nach Nachrichten ist in unverschlüsselten Räumen ohne Problem möglich, da
Element auf alle Nachrichten Zugriff hat. Anders sieht es in verschlüsselten Räumen aus.
Diese Funktion ist nur unter Verwendung des Desktop-Clients möglich (siehe [Element Desktop](/clients/desktop)), da die Nachrichten für die Suche
zwischengespeichert werden müssen. Die Nachrichtensuche muss explizit unter
`Einstellungen` -> `Sicherheit & Datenschutz` -> `Nachrichtensuche` aktiviert werden.
Anschließend ist das Suchen auch in verschlüsselten Räumen möglich.

![Einstellungsebereich für das Aktivieren der Nachrichtensuche](/images/message-search-active_de.png)

