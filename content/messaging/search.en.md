---
title: "Message search"
date: 2021-03-02T11:42:35+01:00
draft: false
chapter: true
weight: 40
---

## Message search

Searching for messages is possible in unencrypted rooms without any problem, since Element
has access to all messages. The situation is different in encrypted rooms.  This function
is only possible using the desktop client (see [Element Desktop](/en/clients/desktop), because the messages must be cached for the search. The message search
must be explicitly enabled under `Settings` -> `Security & Privacy` -> `Message search`.
Afterwards, searching is also possible in encrypted rooms.

![Settings section for enabling message search](/images/message-search-active_en.png)
