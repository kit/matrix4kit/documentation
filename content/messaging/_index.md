---
title: "Nachrichten"
date: 2020-07-15T18:10:07+02:00
draft: false
chapter: true
weight: 30
---

## Personen finden und direkte Nachrichten versenden

Um einzelne Personen anzuschreiben und somit einen 1:1 Chat zu erzeugen ist als Erstes auf das + in der Kategorie „Personen“ zu klicken:

![Klick au den Chat starten Button](/images/01_Messaging_Start_Chat_de.png)

Nun ist in das Suchfeld zu tippen und der Name oder das Kürzel der gesuchten Person einzugeben.

Personen, die schon einen Account in Matrix haben sind, oft auch durch ihren Klarnamen (meist „Vorname Nachname“) auffindbar, sofern sie den Anzeigenamen geändert haben.

Wenn Sie niemanden finden können, fragen Sie nach deren Benutzernamen oder teilen Sie Ihren eigenen Benutzernamen (@ab1234:kit.edu bzw. @uxxxx:kit.edu), damit die angesprochene Person Sie innerhalb von Matrix kontaktieren kann.

Personen, die noch nicht in Matrix eingeloggt waren, sind nicht auffindbar.

Im Suchergebnis ist auf die Zielperson zu klicken und dann auf Los:

![Ein Suchergebnis auf eingegebenen Suchanfrage](/images/02_Messaging_Search_de.png)

Es öffnet sich das Gespräch, welches nach Annahme der Einladung durch die verbundene Person [Ende-zu-Ende-verschlüsselt](/encryption) (inzwischen Standard) beginnen kann. Die Verbindung zum Server am KIT ist natürlich auch transport-verschlüsselt. Sollten Sie aus einem speziellen Grund explizit keine Ende-zu-Ende-Verschlüsselung wünschen, wäre ein unverschlüsselter [Raum zu erzeugen](/rooms/create) und die Gesprächspartner:in in diesen einzuladen.

Weiteres: [Nachrichten formatieren](/formatting)

Ein Raum mit sich selbst ist auch möglich und kann als Zwischenablage / Notizbuch und für Tests benutzt werden, bspw. ob Formatierungen und Hyperlinks korrekt aussehen.
