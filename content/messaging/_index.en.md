---
title: "Messages"
date: 2020-07-15T18:10:07+02:00
draft: false
chapter: true
weight: 30
---

## Find people and send direct messages

To write to individual persons and thus create a 1:1 chat, first click on the + in the category "People":

![Click on the Start Chat button](/images/01_Messaging_Start_Chat_en.png)

Now type in the search field and enter the name or the acronym of the person you are looking for.

People who already have an account in Matrix can often be found by their real name (usually "first name last name"), if they have changed the display name.

If you can't find anyone, ask for their username or share your own username (@ab1234:kit.edu or @uxxxx:kit.edu) so the person you are talking to can contact you within Matrix.

Persons who were not logged in yet are not findable.

In the search result, click on the target person and then on Go:

![A search result for the search query entered](/images/02_Messaging_Search_en.png)

The conversation opens, which can begin after the connected person accepts the invitation [end-to-end encrypted](/en/encryption) (meanwhile standard).
The connection to the server at the KIT is of course also transport-encrypted. If you explicitly do not want end-to-end encryption for a special reason, 
an unencrypted room and invite the conversation partners to it.

Further: [formatting messages](/en/messaging/formatting)

A room with itself is also possible and can be used as clipboard / notebook and for tests, e.g. whether formatting and hyperlinks look correct.

