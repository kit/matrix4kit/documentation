---
title: "Important settings"
date: 2020-07-03T13:22:13+02:00
draft: false
chapter: true
weight: 10
---
# Recommendations for steps after the first login

## Change display name and profile picture

After the successful initial registration, you should change your display name. To do this, click on the top left on
your profile picture and then on **All Settings**. There you can set your **Display Name** e.g. to "**first name last name**".
This allows other people within KIT to find you under your real name.
On the right you can also upload your profile picture.
Afterwards click on Save.
![User Information](/images/01_Browser_User_Information_en.png)

## Notifications

If you want to enable desktop notifications click on your profile picture in the top left corner and then on **Notifications**. There activate "Enable Desktop notifications for this session".
![Screenshot push notifications to enable](/images/01_Browser_Notifications_en.png)
This can also be undone later.

If you also want to receive notifications via email, you also need to enable "Enable email notifications for <_YOUR_ACRONYM_>@sysmail.kit.edu". 
This e-mail address has been set for you in the background and cannot be changed. You will receive all mails sent to this e-mail address as usual in your KIT e-mail inbox.

![Screenshot email notifications to enable](/images/02_Browser_Notifications_en.png)

## Security
In the **Security & Privacy** tab you will find all your devices that have been used by the Matrix account so far.

If necessary, remove sessions that are no longer in use by checking the square box at the beginning of the row and clicking the red button "sign out selected devices".

## Key backup
Unless set up after initial login: The **Secure Backup** is a valuable achievement as it allows,
to save the keys of all conversations, which are end-to-end encrypted, with a password centrally on the KIT server.

To set up the key backup, click **Set up Secure Backup** in the **Security & Privacy** tab.

Press Enter a Security Phrase.
![Prompt to generate a security key or enter a security phrase](/images/02_Browser_Setup_Encryption_Keys_en.png)
Now enter a security phrase that is NOT your KIT password. Please remember it very well.
It grants access to your encrypted messages.
![Prompt to enter a security phrase for the key backup](/images/03_Browser_Security_Phrase_en.png)
Alternatively, instead of the security phrase, you can also have a security key generated that serves the same purpose as the security phrase.
Furthermore, the security key is generated in addition to the security phrase and should be kept safe and retrievable as an emergency key (e.g. save it as .txt file AND print it out)
![Display of the security key to write or save away](/images/04_Browser_Security_Key_en.png)

If you have any questions, please feel free to contact us in the [#helpdesk:kit.edu](https://matrix.to/#/#helpdesk:kit.edu) room.
