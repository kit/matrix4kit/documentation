---
title: "Wichtige Einstellungen"
date: 2020-07-03T13:22:13+02:00
draft: false
chapter: true
weight: 10
---
# Empfehlungen zu Schritten nach dem Erstlogin

## Anzeigename und Profilbild ändern
Nach der erfolgreichen Erst-Anmeldung sollten Sie Ihren Anzeigenamen ändern. Klicken Sie hierfür oben links auf
Ihr Profilbild und dann auf **Alle Einstellungen**. Dort können Sie Ihren **Anzeigenamen** z.B. auf "**Vorname Nachname**" setzen.
Dies ermöglicht anderen Personen innerhalb des KIT Sie unter Ihrem Klarnamen zu finden.
Rechts können Sie außerdem Ihr Profilbild hochladen.
Klicken Sie anschließend auf Speichern.
![User Informationen einstlelen](/images/01_Browser_User_Information_de.png)

## Benachrichtigungen

Um Desktopbenachrichtigungen zu aktivieren, klicken Sie oben links auf Ihr Profilbild und dann auf **Benachrichtigungen**. Dort aktivieren Sie "Desktopbenachrichtigungen in dieser Sitzung".
![Screenshot Push Benachrichtigungen zu aktivieren](/images/01_Browser_Notifications_de.png)
Dies kann später auch rückgängig gemacht werden.

Wenn Sie außerdem Benachrichtigungen per E-Mail erhalten wollen, müssen Sie außerdem "E-Mail-Benachrichtigungen für <_IHR_KÜRZEL_>@sysmail.kit.edu aktivieren" aktivieren. Diese E-Mail-Adresse wurde im Hintergrund für Sie festgelegt und kann nicht geändert werden - die E-Mails erhalten Sie wie gewohnt in Ihrem KIT-Mailpostfach.

![Screenshot E-Mail Benachrichtigungen zu aktivieren](/images/02_Browser_Notifications_de.png)

## Sicherheit
Im Reiter **Sicherheit** finden Sie alle Ihre Geräte, die bisher vom Matrix-Account genutzt wurden. 

Entfernen Sie ggf. nicht mehr benutzte Sitzungen durch Markierung der quadratischen Box am Zeilenanfang und dem Klick auf den roten Button "ausgewählte Geräte abmelden".

## Schlüsselsicherung 
Sofern nicht nach Erstanmeldung eingerichtet: Die **Schlüsselsicherung** ist eine wertvolle Errungenschaft, da Sie ermöglicht, 
die Schlüssel aller Gespräche, die Ende-zu-Ende-verschlüsselt sind, mit einem Passwort versehen zentral auf dem Server 
des KIT zu sichern. 

Um die Schlüsselsicherung einzurichten, klicken Sie im Reiter **Sicherheit** auf **Schlüsselsicherung einrichten**.

Drücken Sie auf Sicherheitsphrase eingeben.
![Aufforderung den Sicherheitsschlüssel zu generieren oder eine Sicherheitsphrase einzugeben](/images/02_Browser_Setup_Encryption_Keys_de.png)
Geben Sie nun eine Sicherheitsphrase ein, die NICHT Ihr KIT-Passwort ist. Bitte merken Sie sich diese sehr gut.
Sie gewährt Zugang zu Ihren verschlüsselten Nachrichten.
![Aufforderung eine Passwort für die Schlüsselsicherung einzugeben](/images/03_Browser_Security_Phrase_de.png)
Alternativ können Sie sich statt der Sicherheitsphrase auch einen Sicherheitsschlüssel generieren lassen,
welcher den selben Zweck wie die Sicherheitsphrase erfüllt. Weiterhin wird der Sicherheitsschlüssel immer zusätzlich
zur Sicherheitsphrase erstellt und sollte als Notfallschlüssel sicher und wiederauffindbar verwahrt werden (z.B. Abspeichern als .txt-Datei UND Ausdrucken)
![Anzeige des Sicherheitsschlüssel zum abschreiben oder wegspeichern](/images/04_Browser_Security_Key_de.png) 

Bei Fragen melden Sie sich gerne im Raum [#helpdesk:kit.edu](https://matrix.to/#/#helpdesk:kit.edu).
