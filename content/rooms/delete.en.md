---
title: "Delete rooms"
date: 2020-07-02T21:23:14+02:00
chapter: true
draft: false
weight: 30
---
## Delete rooms and leave rooms


When leaving rooms, they no longer appear in the room list (left column). 
If the last person in the room leaves the room, the room will be deleted permanently after a short period of time. 

![Room settings with the selection Leave](/images/01_Rooms_Leave_en.png)

As administrator, all room members should first be "kicked" (removed from the room), 
then the person is the last to leave the room and thus initiates the later deletion by the server.
