---
title: "Share rooms"
date: 2020-07-02T21:23:14+02:00
chapter: true
draft: false
weight: 40
---
# Share rooms
{{% notice warning %}}
Sharing rooms only works if they have first been set to "Public" and a room address has been assigned.
See [Create rooms](/en/rooms/create/) for further information.
{{% /notice %}}

**Suggestion Nr. 1 for sharing a room address:**

When you want to share a room within matrix, you can use the internal link feature by typing (the first characters of) the local address
```
#room_address:kit.edu
```
into the chat line. If you are a member of that room, you can confirm the auto-completion with a mouse click. This is a special link for a usage within matrix, which will open directly in the client of the receiver (with a mouse click).


**Suggestion Nr. 2 for sharing a room address:**

{{% notice tip %}}
If you do not like your share links to start with https://matrix.to/..., you can manually create links on the website https://to.matrix.kit.edu.
Until now (as of december 2022), no privacy issues are known about the website https://matrix.to. In particular, the operator of that site does *not* learn anything about the room addresses you share. Still, you can use the site operated by KIT as "more secure alternative".
{{% /notice %}}

The share icon at the top right of each room also offers a matrix.to-link, as well as a QR code and various social networks. The matrix.to-link leads to a page where you can select how the link should be opened. For example, the installed client Element Desktop can be used, or it can be selected via which home server the room is to be entered. 

![share icon marked in the chat view of the room](/images/04_Sharing-Button_en.png)

```
https://matrix.to/#/#Room-address-name:kit.edu?via=kit.edu
```

**Suggestion Nr. 3 for sharing a room address:**

Furthermore, you also can create a hyperlink to the room, which you have to construct in this way:

https://element.matrix.kit.edu/#/room/#roomaddress:kit.edu

resulting in an internet address (URL) which can be easily distributed to the public or target group. BUT, **this link opens only an Element Web in the browser** of the people, not in an installed Element Desktop. More universal (espc. for the large group of people with Element Desktop), and more advised by the Matrix-Admin-Team, is the above mentioned method with the matrix.to-Link.
