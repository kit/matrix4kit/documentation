---
title: "Räume erstellen"
date: 2020-07-02T21:23:14+02:00
chapter: true
draft: false
weight: 10
---
## Räume erstellen und Verantwortung übernehmen

Neue Räume werden über das + in der linken Leiste in der Kategorie Räume erstellt.

![Makierung des Raumhinzufügenbuttons](/images/01_Rooms_Create_de.png)
Anschließend ist der Raumname zu vergeben. Auch kann optional ein Thema (das später öfter angepasst werden kann) vergeben werden. 
Optional kann der Raum auch öffentlich zugänglich gemacht werden (dies ist nicht die Standardeinstellung, siehe unten).
Mit einem zusätzlichen Klick auf „Erweiterte Einstellungen“ kann anschließend noch verhindert werden, 
dass Matrix-Nutzende von außerhalb des KIT Heimatservers (Homeservers) den Raum betreten können. Standard ist inzwischen, 
dass alle neuen Räume (genau wie alle neuen 1:1-Gespräche) eine [Ende-zu-Ende-Verschlüsselung](/encryption) eingerichtet haben.

![Eingabemenü für den Raumnamen](/images/02_Rooms_Name_de.png)

Wenn Sie einen öffentlichen Raum erstellen wollen, wählen Sie "Öffentlicher Raum". Anschließend müssen Sie eine Raumadresse eingeben. 
Unter dieser Adresse wird der Raum dann auffindbar sein. Weitere Informationen, wie Sie Ihren öffentlichen Raum teilen können, finden Sie unter [Räume teilen](/rooms/sharing).

![Eingabemenü für den Raumnamen Öffentlich](/images/03_Rooms_Create_Public_de.png)

Der Raum ist nun erstellt und erhält eine beliebige bunte Icon-Farbe. Durch Klick auf das i oben rechts und anschließend 
das Zahnrad „Raumeinstellungen“ gelangt man zu den Raumeinstellungen:

![Makierung des Raumenstellungsbuttons für den neu erstellten Raum](/images/04_Rooms_Open_Settings_de.png)

Hier kann im Reiter **Allgemein** ein raumspezifisches Bild/Icon hochgeladen werden, sowie der Name und das Raumthema geändert werden.

![Raumeinstellungen](/images/05_Rooms_Settings_de.png)

Im Reiter **Sicherheit** sind für Raumadministrator:innen wichtige Entscheidungen zu treffen: Wer darf Zugang erhalten? Und wer darf den bisherigen Chatverlauf lesen?

![Sicherheitseinstellungen für den neu erstellten Raum](/images/06_Rooms_Settings_de.png)

**Zur Erklärung der Raumzugangsoptionen:**

1. „Privat (Beitreten mit Einladung)“: Das sind geschlossene Räume. Zugang nur durch explizite Einladung.
2. „Spacemitglieder“: Alle Mitglieder ausgewählter [Spaces](/spaces) können dem Raum beitreten.
3. „Öffentlich“: Das ist ein öffentlicher Raum, und lesen können ihn alle. Weltweit. Und Raummitglieder werden nie erfahren, wer es wann gelesen hat. Dies ist also so wie eine Internetseite, auf der alle mitschreiben können. Zu dieser Einstellung oft passend wäre auch die später zu tätigende Option, dass „Jeder“ den Chatverlauf lesen darf.

Ein "Anklopfen" an geschlossene Räume ist bisher nicht möglich. Der nahestehendste Workaround ist, an die raumadministrierende Person eine Direkte Nachricht zu senden, die einen dann einlädt.

{{% notice warning %}}
Als Raum-Administrator:in haben Sie die **Verantwortung** für die im Raum geteilten Inhalte (bspw. Falschnachrichten, Hetze etc.).
Binden Sie weitere Personen in diese Verantwortung ein, in dem Sie in der rechten Leiste (nach Klick auf das Personen) 
über das Drop-Down-Menü Rollen vergeben, bspw. zu Administrator:innen oder Moderator:innen.
{{% /notice %}}

![Usermanagement](/images/03_Rooms_Manage_User_de.png)

Über die Admin-Werkzeuge können Sie auch auf etwaiges Fehlverhalten reagieren (Stummschalten, Kicken, Verbannen, Kürzliche Nachrichten löschen).


