---
title: "Räume teilen"
date: 2020-07-02T21:23:14+02:00
chapter: true
draft: false
weight: 40
---
# Räume teilen
{{% notice warning %}}
Das Teilen von Räumen funktioniert nur, wenn diese zunächst auf "Öffentlich" gestellt wurden und eine Raum-Adresse vergeben wurde.
Siehe hierzu [Räume erstellen](/rooms/create/).
{{% /notice %}}

**Möglichkeit 1 für das Teilen einer Raumadresse:**

Wenn innerhalb von Matrix auf einen anderen Raum verlinkt werden soll, kann dies einfach durch (beginnendes) Eintippen in das Nachrichtenfeld von
```
#Raumadressname:kit.edu
```
geschehen. Ist man selber Mitglied in dem Raum, so erscheinen automatisch Vorschläge. Dies ist dann ein spezieller Hyperlink innerhalb von Matrix, der auch direkt im Client der empfangenden Person (mit einem Mausklick) genutzt werden kann.

Erhält man solche Adressen auf anderem Wege (z.B. via E-Mail) hilft es, diese in einen persönlichen Notizraum (selbst angelegter Raum ohne weitere Personen) zu senden. Anschließend kann man dann bequem auf diesen sich ergebenden Raumlink klicken.

**Möglichkeit 2 für das Teilen einer Raumadresse:**

{{% notice tip %}}
Falls Sie Wert darauf legen, dass der Link, den Sie Anderen schicken, nicht mit https://matrix.to/... beginnt, können Sie manuell Links auf https://to.matrix.kit.edu erstellen.
Es sind derzeit (Stand Dezember 2022) keinerlei Datenschutzprobleme über die Website https://matrix.to bekannt, insbesondere erfährt der Website-Betreiber *nicht*, welche Raum-Links erstellt werden. Dennoch kann die durch das KIT betriebene Seite als "sicherere Alternative" genutzt werden.
{{% /notice %}}

Das Teilen-Symbol oben rechts in jedem Raum, bietet einen matrix.to-Link an, sowie einen QR-Code und verschiedene soziale Netzwerke. Der matrix.to- Link führt auf eine Seite, auf der ausgewählt werden kann, wie der Link geöffnet werden soll. So kann z.B. der installierte Client Element Desktop verwendet werden, oder ausgewählt werden, über welchen Heimserver der Raum betreten werden soll. 

![Teilensymbol in der Chatansicht des Raums makiert](/images/04_Sharing-Button_de.png)

```
https://matrix.to/#/#Raumadressname:kit.edu?via=kit.edu
```

**Möglichkeit 3 für das Teilen einer Raumadresse:**

Weiterhin könnte man aus der vergebenen Raumadresse eine Internetadresse (URL) nach folgender Struktur konstruieren:

https://element.matrix.kit.edu/#/room/#Raumadressname:kit.edu

Diese könnte, ähnlich des matrix.to-Links, auch leicht in der Öffentlichkeit bzw. an die Zielgruppe verteilt werden, **öffnet sich allerdings nur in Element Web**, nicht in einem ggf. installiertem Element Desktop. Universeller (insb. für die große Gruppe an Personen mit Element Desktop) und inzwischen empfehlenswerter ist daher der zuvor beschriebene Weg mit dem matrix.to-Link.





