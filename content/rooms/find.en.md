---
title: "find rooms"
date: 2020-07-02T21:23:14+02:00
chapter: true
draft: false
weight: 20
---
## Find rooms

The room directory, in which the publicly accessible rooms are presented, can be reached via the + in the left bar
and then click on "Explore public rooms".

![Open the menu to find public rooms](/images/01_Rooms_Explore_en.png)

Here you can now search for public rooms and join them by clicking on "Join".

![Menu to find public rooms](/images/02_Rooms_Explore_en.png)

{{% notice note %}}
The global federation is currently in test operation. It is possible that the Federation will be restricted again at a later date.
{{% /notice %}}
