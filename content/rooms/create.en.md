---
title: "Create rooms"
date: 2020-07-02T21:23:14+02:00
chapter: true
draft: false
weight: 10
---
## Create rooms and take responsibility

New rooms are created using the + in the left bar in the category Rooms.

![room add button](/images/01_Rooms_Create_en.png)
Afterwards the room name has to be assigned. Also, optionally a topic (which can be adjusted later more often) can be assigned.
Optionally, the room can also be made publicly accessible (this is not the default setting, see below).
With an additional click on "Show advanced" afterwards it can still be prevented,
that Matrix users from outside the KIT home server can enter the room. Standard is meanwhile,
that all new rooms (just like all new 1:1 calls) have [end-to-end encryption](/en/encryption) set up.

![Input menu for the room name](/images/02_Rooms_Name_en.png)

If you want to create a public room, select "Public room". Then you have to enter a room address.
The room will then be discoverable under this address. For more information on how to share your public room, see [Share rooms](/en/rooms/sharing).

![Input menu for the room name Public](/images/03_Rooms_Create_Public_en.png)

The room is now created and gets any colorful icon color. By clicking on the i in the upper right corner and then
the gear "Room settings" you get to the room settings:

![Macro of the room settings button for the newly created room](/images/04_Rooms_Open_Settings_en.png)

Here you can upload a room specific image/icon in the **General** tab, as well as change the name and room theme.

![Room Settings](/images/05_Rooms_Settings_en.png)

In the tab **Security & Privacy** important decisions have to be made for room administrators: Who is allowed access? And who is allowed to read the previous chat history?

![Security settings for the newly created room](/images/06_Rooms_Settings_en.png)

**To explain the room access options:**.

1. "Private (invite only)": These are closed rooms. Access only by explicit invitation.
2. "Space members": All members of selected [spaces](/en/spaces) can join the room.
3. "Public": This is a public space, and everyone can read it. Worldwide. And space members will never know who read it and when. So this is like a website where everyone can take notes. Often fitting this setting would also be the later option to allow "anyone" to read the chat history.

"Knocking" to closed rooms is not possible so far. The closest workaround is to send a direct message to the person administering the room, who then invites you.

{{% notice warning %}}
As the room administrator, you have **responsibility** for the content shared in the room (e.g. fake news, rants, etc.).
Include other persons in this responsibility by selecting the following in the right bar (after clicking on the person)
drop-down menu to assign roles, e.g. administrator or moderator.
{{% /notice %}}

![User Management](/images/03_Rooms_Manage_User_en.png)

You can also use the admin tools to react to any misbehavior (mute, kick, ban, delete recent messages).
