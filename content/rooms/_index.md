---
title: "Räume"
date: 2020-07-02T21:23:14+02:00
chapter: true
draft: false
weight: 35
---
# Räume 

Konversationen sind in Matrix in Räumen organisiert. 1:1-Gespräche sind auch Räume, aber eben erstmal mit nur zwei Personen. In dieser 
Rubrik geht es um 
1. [Räume erstellen](/rooms/create)
1. [Räume finden](/rooms/find)
1. [Räume löschen](/rooms/delete)
1. [Räume teilen](/rooms/sharing)

